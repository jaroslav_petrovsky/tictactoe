/**
 * @file    Randominator.h
 * @brief   Randominator is AI which places its symbol completely randomly
 *
 * @author  Jaroslav Petrovsky
 * @date    25. October 2015
 */

#ifndef RANDOMINATOR_H
#define RANDOMINATOR_H

#include "AIInterface.h"

class Randominator : public AIInterface
{
    public:
        Randominator(int computerSymbol, int rowsNumber, int collsNumber, QObject* parent = nullptr);

        void think(const Grid grid, int rowPlayer, int collPlayer, int &rowComputer, int &collComputer);
};

#endif // RANDOMINATOR_H
