/**
 * @file    Playground.cpp
 * @brief   Playground displays TicTacToe 3x3 area and computer/user inputs.
 *          No game logic or AI, just graphical representation of game progress.
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#include "Playground.h"
#include "Settings.h"

#include <QPainter>
#include <QMouseEvent>
#include <QMutex>

Playground::Playground(Settings* settings, QWidget* parent) :
    QWidget(parent),
    m_settings(settings),
    m_makingMove(false),
    m_gameOver(false),
    m_clickedRow(-1),
    m_clickedColl(-1),
    m_grid(nullptr),
    m_winningLane(0, 0, 0, 0)
{
    reset();
}

Playground::~Playground()
{
    // do not delete, not owner
    m_settings = nullptr;
}

void Playground::updateGrid(Grid grid)
{
    // grid could be being redrawn
    m_gridMutex.lock();

    m_grid = grid;

    m_gridMutex.unlock();

    repaint();
}

void Playground::reset()
{
    loadSettings();

    // when reset button is hit, this reset is called and grid is not yet updated from
    // gamecontroller, so if there is more total cell than before reset, app will crash
    // when drawMarks access elements of grid which not yet exist
    initGrid();

    m_gameOver = false;
    m_winningLane = GameController::Lane(0, 0, 0, 0);

    repaint();
}

void Playground::winner(int fromRow, int fromCell, int toRow, int toCell)
{
    m_gameOver = true;

    m_winningLane = GameController::Lane(fromRow, fromCell, toRow, toCell);

    repaint();
}

void Playground::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);

    drawGrid(painter);
    drawSymbols(painter);

    if(m_gameOver)
        drawWinningLane(painter);
}

void Playground::mousePressEvent(QMouseEvent* event)
{
    if(m_gameOver)
        return;

    if(event->button() == Qt::LeftButton)
    {
            calculateClickedRowAndColl(event->pos(), m_clickedRow, m_clickedColl);
            m_makingMove = true;
    }
}

void Playground::mouseReleaseEvent(QMouseEvent* event)
{
    if(m_gameOver)
        return;

    int clickedRow;
    int clickedColl;

    calculateClickedRowAndColl(event->pos(), clickedRow, clickedColl);

    if(event->button() == Qt::LeftButton &&
       m_makingMove == true              &&
       (clickedRow == m_clickedRow && clickedColl == m_clickedColl))
    {
            emit playerMoves(m_clickedRow, m_clickedColl);
    }

    m_makingMove = false;
    m_clickedRow = -1;
    m_clickedColl = -1;
}

void Playground::loadSettings()
{
    if(m_settings != nullptr)
    {
        m_rowsCount     = m_settings->getRowsCount();
        m_collsCount    = m_settings->getCollsCount();
    }
}

void Playground::initGrid()
{
    // to make sure update from outside is not called
    m_gridMutex.lock();

    if(m_grid == nullptr)
        m_grid = QSharedPointer<QVector<int>>(new QVector<int>());

    int size = m_rowsCount * m_collsCount;

    m_grid->reserve(size);

    std::fill_n(m_grid->begin(), size, GameController::Symbol::Empty);

    m_gridMutex.unlock();
}

void Playground::drawGrid(QPainter &painter)
{
    int width           = this->width();
    int height          = this->height();
    double cellWidth    = width / (double)m_collsCount;
    double cellHeight   = height / (double)m_rowsCount;

    painter.setPen(Qt::black);

    // draw grid vertical lines
    for(int i = 1; i < m_collsCount; i++)
        painter.drawLine(cellWidth * i, 0, cellWidth * i, height);

    // draw grid horizontal lines
    for(int i = 1; i < m_rowsCount; i++)
        painter.drawLine(0, cellHeight * i, width, cellHeight * i);
}

void Playground::drawSymbols(QPainter &painter)
{
    // to make sure grid will not be updated during drawing
    m_gridMutex.lock();

    if(m_grid == nullptr || m_grid->empty())
    {
        m_gridMutex.unlock();
        return;
    }

    for(int coll = 0; coll < m_collsCount; coll++)
    {
        for(int row = 0; row < m_rowsCount; row++)
        {
            int symbol = (*m_grid)[coll * m_rowsCount + row];

            drawSymbol(painter, symbol, row, coll);
        }
    }

    m_gridMutex.unlock();
}

void Playground::drawSymbol(QPainter &painter, int symbol, int row, int coll)
{
    int width           = this->width();
    int height          = this->height();
    double cellWidth    = width / (double)m_collsCount;
    double cellHeight   = height / (double)m_rowsCount;

    if(symbol == GameController::Symbol::Cross)
    {
        // first line of X
        double x1 = coll * cellWidth + (cellWidth * 0.1);
        double y1 = row * cellHeight  + (cellHeight * 0.1);
        double x2 = (coll + 1) * cellWidth - (cellWidth * 0.1);
        double y2 = (row + 1) * cellHeight - (cellHeight * 0.1);

        painter.drawLine(x1, y1, x2, y2);

        // second line of X
        x1 = (coll + 1) * cellWidth - (cellWidth * 0.1);
        y1 = row * cellHeight  + (cellHeight * 0.1);
        x2 = coll * cellWidth + (cellWidth * 0.1);
        y2 = (row + 1) * cellHeight - (cellHeight * 0.1);

        painter.drawLine(x1, y1, x2, y2);
    }
    else if(symbol == GameController::Symbol::Circle)
    {
        double x = coll * cellWidth + (cellWidth * 0.1);
        double y = row * cellHeight + (cellHeight * 0.1);

        painter.drawEllipse(x, y, cellWidth - (cellWidth * 0.2), cellHeight - (cellHeight * 0.2));
    }
}

void Playground::drawWinningLane(QPainter &painter)
{
    int width           = this->width();
    int height          = this->height();
    double cellWidth    = width / (double)m_collsCount;
    double cellHeight   = height / (double)m_rowsCount;

    double x1 = m_winningLane.fromColl * cellWidth + (cellWidth/2.0);
    double y1 = m_winningLane.fromRow * cellHeight  + (cellHeight/2.0);
    double x2 = m_winningLane.toColl * cellWidth + (cellWidth/2.0);
    double y2 = m_winningLane.toRow * cellHeight  + (cellHeight/2.0);

    painter.drawLine(x1, y1, x2, y2);
}

void Playground::calculateClickedRowAndColl(QPoint cursorPosition, int &row, int &coll)
{
    int width           = this->width();
    int height          = this->height();
    double cellWidth    = width / (double)m_collsCount;
    double cellHeight   = height / (double)m_rowsCount;

    coll = cursorPosition.x() / cellWidth;
    row = cursorPosition.y() / cellHeight;
}
