/**
 * @file    AIInterface.h
 * @brief   Interface for different AI classes. AI just needs to know how to "think"
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#ifndef AIINTERFACE_H
#define AIINTERFACE_H

#include "GameController.h"

#include <QObject>

class AIInterface : public QObject
{
    Q_OBJECT
    public:
        explicit AIInterface(QObject* parent = nullptr);
        virtual ~AIInterface() = default;

        /*
         * This function "thinks" of next computer move.
         * Reference parameters row and column are filled with values,
         * which should be used for next computer move.
         * Grid is locked before sending into this function, so the user does not
         * have to be afraid that it will be changed during "thinking"
         */
        virtual void think(const Grid grid, int rowPlayer, int collPlayer, int &rowComputer, int &collComputer) = 0;

    protected:
        // game settings
        int m_computerSymbol;
        int m_playerSymbol;
        int m_rowsNumber;
        int m_collsNumber;
};

#endif // AIINTERFACE_H
