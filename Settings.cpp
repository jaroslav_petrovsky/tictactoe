/**
 * @file    Settings.h
 * @brief   Settings window allows player to choose his weapon of choice and who starts.
 *
 * @author  Jaroslav Petrovsky
 * @date    18. October 2015
 */

#include "ui_SettingsTemplate.h"

#include "Settings.h"
#include "GameController.h"

#include <QDesktopWidget>

Settings::Settings(QWidget* parent) :
    QWidget(parent),
    m_gui(new Ui::SettingsTemplate())
{
    m_gui->setupUi(this);

    // set to center of the screen
    move(QApplication::desktop()->screen()->rect().center() - rect().center());
}

int Settings::getRowsCount() const
{
    return m_gui->rowsCount->value();
}

int Settings::getCollsCount() const
{
    return m_gui->collsCount->value();
}

int Settings::getWinningScore() const
{
    return m_gui->winningScore->value();
}

bool Settings::isRandominator() const
{
    return m_gui->randominator->isChecked();
}

bool Settings::isTerminator() const
{
    return m_gui->terminator->isChecked();
}

GameController::Symbol Settings::getPlayersMark() const
{
    return m_gui->circle->isChecked() ? GameController::Symbol::Circle : GameController::Symbol::Cross;
}

bool Settings::computerStarts() const
{
    return m_gui->computer->isChecked();
}
