/**
 * @file    AIInterface.h
 * @brief   Interface for different AI classes. AI just needs to know how to "think"
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#include "AIInterface.h"

AIInterface::AIInterface(QObject* parent) : QObject(parent)
{

}
