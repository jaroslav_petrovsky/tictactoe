/**
 * @file    main.cpp
 * @brief   Application main function.
 *
 * @author  Jaroslav Petrovsky
 * @date    14. October 2015
 */

#include "MainWindow.h"

#include <QApplication>

int main(int argc, char **argv)
{
    QApplication* app = new QApplication(argc, argv);

    MainWindow* window = new MainWindow();

    window->setUp();

    return app->exec();
}


