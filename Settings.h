/**
 * @file    Settings.h
 * @brief   Settings window allows player to choose his weapon of choice and who starts.
 *
 * @author  Jaroslav Petrovsky
 * @date    18. October 2015
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include "GameController.h"

#include <QWidget>

namespace Ui
{
    class SettingsTemplate;
}

class Settings : public QWidget
{
    Q_OBJECT
    public:
        explicit Settings(QWidget* parent = nullptr);

        GameController::Symbol getPlayersMark() const;
        bool computerStarts() const;
        int getRowsCount() const;
        int getCollsCount() const;
        int getWinningScore() const;

        bool isRandominator() const;
        bool isTerminator() const;

    private:
        Ui::SettingsTemplate* m_gui;
};

#endif // SETTINGS_H
