/**
 * @file    Temrinator.h
 * @brief   Terminator AI does its best to beat player.
 *
 * @author  Jaroslav Petrovsky
 * @date    25. October 2015
 */

#include "Terminator.h"

#include "Randominator.h"
#include "GameController.h"

#include <vector>
#include <queue>

Terminator::Terminator(int computerSymbol, int playerSymbol, int rowsNumber, int collsNumber, QObject* parent) :
    AIInterface(parent)
{
    m_computerSymbol    = computerSymbol;
    m_playerSymbol      = playerSymbol;
    m_rowsNumber        = rowsNumber;
    m_collsNumber       = collsNumber;
}

void Terminator::think(const Grid grid, int rowPlayer, int collPlayer, int &rowComputer, int &collComputer)
{
    // we are making the first move
    if(rowPlayer == -1 && collPlayer == -1)
    {
        Randominator randominator(m_computerSymbol, m_rowsNumber, m_collsNumber, this);
        randominator.think(grid, rowPlayer, collPlayer, rowComputer, collComputer);
        return;
    }

    // check every direction - same like in game controller, but now
    // we check every direction separately

    std::priority_queue<LaneInfo, std::vector<LaneInfo>, LaneInfoCompare> lanes;

    int rowStep = -1;
    int collStep = -1;

    // collect possible player winning lanes
    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            // skip because this gets us nowhere
            if(rowStep == 0 && collStep == 0)
                continue;

            lanes.push(checkDirection(grid, rowPlayer, collPlayer, rowStep, collStep));

            collStep++;
        }

        collStep = -1;
        rowStep++;
    }

    bool symbolPlaced = false;

    while(!symbolPlaced && !lanes.empty())
    {
        LaneInfo blockLane = lanes.top();

        // if opponent symbols form a continual lane, than place our symbol at one of the ends
        // if there is an empty space between symbols, used that empty space
        int fromRow = blockLane.m_lane.fromRow;
        int fromColl = blockLane.m_lane.fromColl;

        int toRow = blockLane.m_lane.toRow;
        int toColl = blockLane.m_lane.toColl;

        rowStep = blockLane.m_rowStep;
        collStep = blockLane.m_collStep;

        // check all cells from start to end and count opponent symbols
        int currentRow      = fromRow;
        int currentColl     = fromColl;

        bool rowReachedEnd  = currentRow == (toRow + rowStep) && rowStep != 0;
        bool collReachedEnd = currentColl == (toColl + collStep) && collStep != 0;

        // traverse lane and put symbol in between opponent symbols, if possible (opponent symbols
        // do not form continual lane)
        while( !( (rowReachedEnd || collReachedEnd) || symbolPlaced) )
        {
            int symbol = (*grid)[currentColl * m_rowsNumber + currentRow];
            if(symbol != m_playerSymbol)
            {
                // if player symbol, it means from this direction the lane is closed
                if(symbol == m_computerSymbol)
                    break;

                rowComputer = currentRow;
                collComputer = currentColl;
                symbolPlaced = true;
            }

            currentRow += rowStep;
            currentColl += collStep;

            rowReachedEnd  = currentRow == (toRow + rowStep) && rowStep != 0;
            collReachedEnd = currentColl == (toColl + collStep) && collStep != 0;
        }

        // opponent symbol form continual lane, put symbol at start or end if possible
        if(symbolPlaced == false)
        {
            currentRow = fromRow - rowStep;
            currentColl = fromColl - collStep;

            // first check start position
            if(currentRow < 0 || currentRow == m_rowsNumber || currentColl < 0 || currentColl == m_collsNumber)
            {
                currentRow = toRow + rowStep;
                currentColl = toColl + collStep;

                // then end position
                if(currentRow < 0 || currentRow == m_rowsNumber || currentColl < 0 || currentColl == m_collsNumber)
                {
                   // if none of them works, proceed to next lane
                    lanes.pop();
                    continue;
                }
            }

            // finally check, whether there is not already another symbol occupying the cell
            if((*grid)[currentColl * m_rowsNumber + currentRow] != GameController::Symbol::Empty)
            {
                lanes.pop();
                continue;
            }

            symbolPlaced = true;

            rowComputer = currentRow;
            collComputer = currentColl;
        }
    }

    // put symbol randomly
    if(symbolPlaced == false)
    {
        Randominator randominator(m_computerSymbol, m_rowsNumber, m_collsNumber, this);
        randominator.think(grid, rowPlayer, collPlayer, rowComputer, collComputer);
    }
}

Terminator::LaneInfo Terminator::checkDirection(const Grid grid, int row, int coll, int rowStep, int collStep)
{
    // setup properly star and end values
    int fromRow = row;
    int fromColl = coll;

    int toRow = row;
    int toColl = coll;
    while(toRow >= 0 && toRow < m_rowsNumber && toColl >= 0 && toColl < m_collsNumber)
    {
        toRow += rowStep;
        toColl += collStep;
    }

    toRow -= rowStep;
    toColl -= collStep;

    // check all cells from start to end and count opponent symbols
    int currentRow      = fromRow;
    int currentColl     = fromColl;
    int symbolsInLane   = 0;

    bool rowReachedEnd  = currentRow == (toRow + rowStep) && rowStep != 0;
    bool collReachedEnd = currentColl == (toColl + collStep) && collStep != 0;

    while( !(rowReachedEnd || collReachedEnd) )
    {
        if((*grid)[currentColl * m_rowsNumber + currentRow] == m_playerSymbol)
        {
            symbolsInLane++;
        }

        currentRow += rowStep;
        currentColl += collStep;

        rowReachedEnd  = currentRow == (toRow + rowStep) && rowStep != 0;
        collReachedEnd = currentColl == (toColl + collStep) && collStep != 0;
    }

    return LaneInfo(GameController::Lane(fromRow, fromColl, toRow, toColl), symbolsInLane, rowStep, collStep);

}
