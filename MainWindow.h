/**
 * @file    MainWindow.cpp
 * @brief   Application main window which consists of menu and playground widget.
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui
{
    class MainWindowTemplate;
}
class Playground;
class GameController;
class Settings;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        ~MainWindow();

        void setUp();

    protected:
        void closeEvent(QCloseEvent* event);

    private:
        Ui::MainWindowTemplate* m_gui;
        Settings*               m_settings;
        Playground*             m_playground;
        GameController*         m_gameController;
};

#endif // MAINWINDOW_H
