/**
 * @file    GameController.cpp
 * @brief   GameController controlls mouse input from player, computer ai and game results.
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QMutex>
#include <QObject>
#include <QVector>
#include <QSharedPointer>
#include <QGenericMatrix>
#include <QSharedPointer>

// in memory, grid is represented like normal array, but logically single dimension array layout
// is used for 2-dimensional array
// data: cell symbol (whether player, computer or no one occupies that cell)
// so to get symbol at row x and cell y symbol = m_grid[x * m_rowsCount + y]
// possible uprade: use proxy object to hide this computation
typedef QSharedPointer<QVector<int>> Grid;

class AIInterface;
class Settings;
class QMutex;

class GameController : public QObject
{
    Q_OBJECT
    public:
        struct Lane
        {
            Lane(int fromRow, int fromColl, int toRow, int toColl) :
                fromRow(fromRow), fromColl(fromColl), toRow(toRow), toColl(toColl)
            {
            }

            int fromRow;
            int fromColl;
            int toRow;
            int toColl;
        };

        explicit GameController(Settings* settings, QObject* parent = nullptr);
        ~GameController();

        // enum for different symbol values in grid
        enum Symbol
        {
            Empty,
            Circle,
            Cross
        };

    signals:
        void gridUpdated(Grid grid);
        void winner(int fromRow, int fromCell, int toRow, int toCell);

    public slots:
        void processPlayersMove(int row, int coll);
        void reset();

    private:
        void loadSettings();
        void makeMove(int rowPlayer, int collPlayer, int& rowComputer, int& collComputer); // first row/coll is player move, second our move
        void checkVictory(int row, int coll, int symbol);
        void initGrid();
        Lane checkDirection(int row, int coll, int rowStep, int collStep, int rowsNumber, int collsNumber, int winningScore, int symbol);
        bool checkLane(const Lane& lane);

    private:
        Settings*       m_settings;
        AIInterface*    m_ai;

        Grid            m_grid;
        int             m_rowsCount;\
        int             m_collsCount;
        int             m_winningScore;
        int             m_freeCellsRemaining;

        Symbol          m_playersSymbol;
        Symbol          m_computersSymbol;

        bool            m_computerStarts;
        bool            m_gameOver;

        QMutex          m_gridMutex;
};

#endif // GAMECONTROLLER_H
