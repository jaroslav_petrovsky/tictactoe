/**
 * @file    Temrinator.h
 * @brief   Terminator AI does its best to beat player.
 *
 * @author  Jaroslav Petrovsky
 * @date    25. October 2015
 */

#ifndef TERMINATOR_H
#define TERMINATOR_H

#include "AIInterface.h"

class Terminator : public AIInterface
{
    private:
        struct LaneInfo
        {
            LaneInfo(GameController::Lane lane, int opponentSymbolCount, int rowStep, int collStep) :
                m_lane(lane), m_opponentSymbolCount(opponentSymbolCount), m_rowStep(rowStep), m_collStep(collStep)
            {}

            GameController::Lane m_lane;
            int m_opponentSymbolCount;
            int m_rowStep;
            int m_collStep;
        };

        class LaneInfoCompare
        {
            public:
                bool operator() (const LaneInfo& lhs, const LaneInfo& rhs)
                {
                    if(lhs.m_opponentSymbolCount < rhs.m_opponentSymbolCount)
                        return true;

                    return false;
                }
        };

    public:
        Terminator(int computerSymbol, int playerSymbol, int rowsNumber, int collsNumber, QObject* parent = nullptr);

        void think(const Grid grid, int rowPlayer, int collPlayer, int &rowComputer, int &collComputer);

    private:
        LaneInfo checkDirection(const Grid grid, int row, int coll, int rowStep, int collStep);
};

#endif // TERMINATOR_H
