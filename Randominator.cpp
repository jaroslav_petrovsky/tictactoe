/**
 * @file    Randominator.cpp
 * @brief   Randominator is AI which places its symbol completely randomly
 *
 * @author  Jaroslav Petrovsky
 * @date    25. October 2015
 */

#include "Randominator.h"

#include <cstdlib>
#include <ctime>

Randominator::Randominator(int computerSymbol, int rowsNumber, int collsNumber, QObject* parent) :
    AIInterface(parent)
{
    m_computerSymbol = computerSymbol;
    m_rowsNumber = rowsNumber;
    m_collsNumber = collsNumber;
}

void Randominator::think(const Grid grid, int rowPlayer, int collPlayer, int &rowComputer, int &collComputer)
{
    std::srand(std::time(nullptr));

    // search until we find empty cell
    do
    {
        rowComputer     = std::rand() % m_rowsNumber;
        collComputer    = std::rand() % m_collsNumber;
    }
    while((*grid)[collComputer * m_rowsNumber + rowComputer] != GameController::Symbol::Empty);
}
