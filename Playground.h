/**
 * @file    Playground.cpp
 * @brief   Playground displays TicTacToe 3x3 area and computer/user inputs.
 *          No game logic or AI, just graphical representation of game progress.
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#ifndef PLAYGROUND_H
#define PLAYGROUND_H

#include "GameController.h"

#include <QWidget>

class Settings;
class QMutex;

class Playground : public QWidget
{
    Q_OBJECT
    public:
        explicit Playground(Settings* settings, QWidget* parent = nullptr);
        ~Playground();

    signals:
        void playerMoves(int row, int coll);

    public slots:
        void updateGrid(Grid grid);
        void reset();
        // not sending Lane struct, because Lane is not QObject and cannot be send via signal/slot system
        void winner(int fromRow, int fromCell, int toRow, int toCell);

    protected:
        void paintEvent(QPaintEvent* event);
        void mousePressEvent(QMouseEvent* event);
        void mouseReleaseEvent(QMouseEvent* event);

    private:
        void loadSettings();
        void initGrid();

        void drawGrid(QPainter& painter);
        void drawSymbols(QPainter& painter);
        void drawSymbol(QPainter& painter, int symbol, int row, int coll);
        void drawWinningLane(QPainter& painter);
        void calculateClickedRowAndColl(QPoint cursorPosition, int& row, int& coll);

    private:
        Settings*       m_settings;

        int             m_rowsCount;\
        int             m_collsCount;

        bool            m_makingMove;
        bool            m_gameOver;
        int             m_clickedRow;
        int             m_clickedColl;
        Grid            m_grid;

        QMutex          m_gridMutex;

        GameController::Lane m_winningLane;

};

#endif // PLAYGROUND_H
