/**
 * @file    GameController.cpp
 * @brief   GameController controlls mouse input from player, computer ai and game results.
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#include "GameController.h"

#include "AIInterface.h"
#include "Settings.h"
#include "Randominator.h"
#include "Terminator.h"

#include <algorithm>

GameController::GameController(Settings* settings, QObject* parent) :
    QObject(parent),
    m_settings(settings),
    m_ai(nullptr),
    m_grid(nullptr),
    m_freeCellsRemaining(0),
    m_computerStarts(false),
    m_gameOver(false)
{
    reset();
}

GameController::~GameController()
{
    if(m_ai != nullptr)
    {
        delete m_ai;
        m_ai = nullptr;
    }

    // do not delete, not owner
    m_settings = nullptr;
}

void GameController::processPlayersMove(int row, int coll)
{
    if(m_gameOver)
        return;

    m_gridMutex.lock();

    if((*m_grid)[coll * m_rowsCount + row] == Symbol::Empty)
    {
        (*m_grid)[coll * m_rowsCount + row] = m_playersSymbol;
        m_freeCellsRemaining--;
    }
    else
    {
        m_gridMutex.unlock();
        return;
    }

    m_gridMutex.unlock();

    checkVictory(row, coll, m_playersSymbol);

    if(m_gameOver)
        return;

    int computerRow;
    int computerColl;

    makeMove(row, coll, computerRow, computerColl);

    checkVictory(computerRow, computerColl, m_computersSymbol);
}

void GameController::reset()
{
    loadSettings();

    initGrid();

    m_gameOver = false;

    int row;
    int coll;

    if(m_computerStarts)
        makeMove(-1, -1, row, coll);
}

void GameController::loadSettings()
{
    if(m_settings != nullptr)
    {
        m_playersSymbol     = m_settings->getPlayersMark();
        m_computersSymbol   = m_playersSymbol == Symbol::Circle ? Symbol::Cross : Symbol::Circle;
        m_computerStarts    = m_settings->computerStarts();

        m_rowsCount     = m_settings->getRowsCount();
        m_collsCount    = m_settings->getCollsCount();
        m_winningScore  = m_settings->getWinningScore();

        if(m_ai != nullptr)
        {
             delete m_ai;
            m_ai = nullptr;
        }

        if(m_settings->isRandominator())
            m_ai = new Randominator(m_computersSymbol, m_rowsCount, m_collsCount, this);

        if(m_settings->isTerminator())
            m_ai = new Terminator(m_computersSymbol, m_playersSymbol, m_rowsCount, m_collsCount, this);
    }
}

void GameController::makeMove(int rowPlayer, int collPlayer, int &rowComputer, int &collComputer)
{
    m_gridMutex.lock();

    m_ai->think(m_grid, rowPlayer, collPlayer, rowComputer, collComputer);

    (*m_grid)[collComputer * m_rowsCount + rowComputer] = m_computersSymbol;
    m_freeCellsRemaining--;

    m_gridMutex.unlock();

    emit gridUpdated(m_grid);
}

void GameController::checkVictory(int row, int coll, int symbol)
{
    // to check for victory we check the all directions in radius of m_winningScore - 1 cells

    // we have 8 possible direction in which we can have a victory strike
    // check all cells in m_winningScore - 1 radius
    // x-x-x
    // -xxx-
    // xxxxx    illustration of all (8) the possible directions you can win
    // -xxx-
    // x-x-x
    //
    // and if you take a closer look, you will see that only 4 of them are unique, other 4 are just mirrored
    // so we do not have to check all of them

    // directions are clockwise from 12 o'clock
    m_gridMutex.lock();

    Lane winningDirection(0, 0, 0, 0);

    Lane direction1 = checkDirection(row, coll, -1,  0, m_rowsCount, m_collsCount, m_winningScore, symbol);
    if(checkLane(direction1))
    {
        m_gameOver = true;
        winningDirection = direction1;
    }

    Lane direction2 = checkDirection(row, coll, -1, +1, m_rowsCount, m_collsCount, m_winningScore, symbol);
    if(checkLane(direction2))
    {
        m_gameOver = true;
        winningDirection = direction2;
    }

    Lane direction3 = checkDirection(row, coll,  0, +1, m_rowsCount, m_collsCount, m_winningScore, symbol);
    if(checkLane(direction3))
    {
        m_gameOver = true;
        winningDirection = direction3;
    }

    Lane direction4 = checkDirection(row, coll, +1, +1, m_rowsCount, m_collsCount, m_winningScore, symbol);
    if(checkLane(direction4))
    {
        m_gameOver = true;
        winningDirection = direction4;
    }

    if(m_gameOver)
        emit winner(winningDirection.fromRow, winningDirection.fromColl, winningDirection.toRow, winningDirection.toColl);
    else
    {
        if(m_freeCellsRemaining == 0)
            m_gameOver = true;
    }

    m_gridMutex.unlock();
}

void GameController::initGrid()
{
    m_gridMutex.lock();

    if(m_grid == nullptr)
        m_grid = QSharedPointer<QVector<int>>(new QVector<int>());

    int size = m_rowsCount * m_collsCount;

    m_grid->resize(size);
    m_grid->reserve(size);

    std::fill_n(m_grid->begin(), size, Symbol::Empty);

    m_freeCellsRemaining = size;

    m_gridMutex.unlock();
}

GameController::Lane GameController::checkDirection(int row, int coll, int rowStep, int collStep, int rowsNumber, int collsNumber, int winningScore, int symbol)
{
    // setup star and end values
    int fromRow = row;
    int fromColl = coll;
    while(fromRow >= 0 && fromRow < rowsNumber && fromColl >= 0 && fromColl < collsNumber)
    {
        fromRow -= rowStep;
        fromColl -= collStep;
    }

    fromRow += rowStep;
    fromColl += collStep;

    int toRow = row;
    int toColl = coll;
    while(toRow >= 0 && toRow < rowsNumber && toColl >= 0 && toColl < collsNumber)
    {
        toRow += rowStep;
        toColl += collStep;
    }

    toRow -= rowStep;
    toColl -= collStep;

    int rowDistance = fromRow > toRow ? fromRow - toRow : toRow - fromRow;
    int collDistance = fromColl > toColl ? fromColl - toColl : toColl - fromColl;

    // if difference between start/end values for both row and coll is less than winnig score
    // than there is no space to create winning strike of winningScore symbols in lane
    if( (rowDistance < (winningScore - 1)) && (collDistance < (winningScore - 1)) ) // -1 because indexing starts at 0
        return Lane(0, 0, 0, 0);

    // check all cells from start to end or until we reach winningScore of symbol in a lane
    int currentRow      = fromRow;
    int currentColl     = fromColl;
    int symbolsInLane   = 0;
    int startRow        = currentRow;
    int startColl       = symbolsInLane;
    int endRow          = currentRow;
    int endColl         = symbolsInLane;

    bool rowReachedEnd  = currentRow == (toRow + rowStep) && rowStep != 0;
    bool collReachedEnd = currentColl == (toColl + collStep) && collStep != 0;
    bool win            = symbolsInLane == winningScore;

    while( !( (rowReachedEnd || collReachedEnd) || win) )
    {
        if((*m_grid)[currentColl * m_rowsCount + currentRow] == symbol)
        {
            // if we got symbol we are searching for, than we extend line by moving end the curren row/cell
            endRow = currentRow;
            endColl = currentColl;

            // if this is first symbol, set start symbols
            if(symbolsInLane == 0)
            {
                startRow = currentRow;
                startColl = currentColl;
            }

            symbolsInLane++;
        }
        else
        {
            symbolsInLane = 0;
        }

        currentRow += rowStep;
        currentColl += collStep;

        rowReachedEnd  = currentRow == (toRow + rowStep) && rowStep != 0;
        collReachedEnd = currentColl == (toColl + collStep) && collStep != 0;
        win            = symbolsInLane == winningScore;
    }

    if(symbolsInLane == winningScore)
        return Lane(startRow, startColl, endRow, endColl);

    return Lane(0, 0, 0, 0);
}

bool GameController::checkLane(const GameController::Lane &lane)
{
    if(lane.fromRow     == 0 &&
       lane.fromColl    == 0 &&
       lane.toRow       == 0 &&
       lane.toColl      == 0)
        return false;

    return true;
}
