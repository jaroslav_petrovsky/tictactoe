/**
 * @file    MainWindow.cpp
 * @brief   Application main window which consists of menu and playground widget.
 *
 * @author  Jaroslav Petrovsky
 * @date    17. October 2015
 */

#include "ui_MainWindowTemplate.h"

#include "MainWindow.h"
#include "Playground.h"
#include "GameController.h"
#include "Settings.h"

#include <QDesktopWidget>

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    m_gui(new Ui::MainWindowTemplate()),
    m_settings(new Settings()),
    m_playground(new Playground(m_settings, this)),
    m_gameController(new GameController(m_settings, this))
{
    m_gui->setupUi(this);
    setCentralWidget(m_playground);

    QObject::connect(m_gui->actionQuit,     SIGNAL(triggered()),                this,               SLOT(close()));
    QObject::connect(m_gui->actionQuit,     SIGNAL(triggered()),                m_settings,         SLOT(close()));
    QObject::connect(m_gui->actionReset,    SIGNAL(triggered()),                m_playground,       SLOT(reset()));
    QObject::connect(m_gui->actionReset,    SIGNAL(triggered()),                m_gameController,   SLOT(reset()));
    QObject::connect(m_gui->actionSettings, SIGNAL(triggered()),                m_settings,         SLOT(show()));

    QObject::connect(m_playground,          SIGNAL(playerMoves(int, int)),      m_gameController,   SLOT(processPlayersMove(int, int)));
    QObject::connect(m_gameController,      SIGNAL(gridUpdated(Grid)),          m_playground,       SLOT(updateGrid(Grid)));
    QObject::connect(m_gameController,      SIGNAL(winner(int,int,int,int)),    m_playground,       SLOT(winner(int,int,int,int)));

    // set to center of the screen
    move(QApplication::desktop()->screen()->rect().center() - rect().center());
}

MainWindow::~MainWindow()
{
    if(m_gui != nullptr)
    {
        delete m_gui;
        m_gui = nullptr;
    }

    if(m_playground != nullptr)
    {
        delete m_playground;
        m_playground = nullptr;
    }

    if(m_gameController != nullptr)
    {
        delete m_gameController;
        m_gameController = nullptr;
    }

    if(m_settings != nullptr)
    {
        delete m_settings;
        m_settings = nullptr;
    }
}

void MainWindow::setUp()
{
    show();

    m_settings->show();

    m_gameController->reset();
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    m_settings->close();
}
